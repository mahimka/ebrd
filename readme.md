# Project Title

One Paragraph of project description goes here

### Installing

```
yarn

```

## Test

```
npm test

```

## Run

```
npm start

```

## Build

```
npm run build

```

## Deploy

```
npm run deploy

```

## Release

```
npm run release

```


## Authors

**Max Kuteynikov**