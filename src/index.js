import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
// import { BrowserRouter } from 'react-router-dom';
import App from './components/app';
import data from './data';

const root = document.getElementById('app-root');

root.setAttribute('data-ua', navigator.userAgent);
ReactDOM.render(
	// <BrowserRouter>
		<App data={data} />
	// </BrowserRouter>
, root);