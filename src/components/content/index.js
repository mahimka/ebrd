import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';

import 'gsap/TweenMax';
import 'imports-loader?define=>false!gsap/src/uncompressed/plugins/ScrollToPlugin';

export default class Content extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    window.scrollTo(0, 0);
  }

  componentDidUpdate(){
  }

  render() { 
    const data = this.props;
    const menu = ["Risks", "Opportunities", "Scenario analaysis"];
    const submenu = ["Disclosures about physical climate opportunities", "Disclosures about physical climate risks", "Using of scenarios in physical climate disclosures"];

    const Icon = ({ icon,  ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/${icon}.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    return (
      <div className="ebrd-content">
          
          <div className="ebrd-content-header-block"> 
            <div className="ebrd-content-header"> {data.header} </div>
            <div id="ebrd-content-icons">
              <Icon icon={`content-icon${data.id}l`} className="ebrd-content-iconl" id={`ebrd-content-icon${data.id}l`}/>
              <Icon icon={`content-icon${data.id}r`} className="ebrd-content-iconr" id={`ebrd-content-icon${data.id}r`}/>
            </div>
          </div>

          <div className="ebrd-content-texts">
            <div id="ebrd-whiteline4"></div>
            <div id="ebrd-whiteline5"></div>
            <div className="ebrd-content-text"> {data.txt0} </div>
            <br/>
            <div className="ebrd-content-text1"> {data.txt1} </div>
          </div>

          <div className="ebrd-content-menu">
            <div id="ebrd-whiteline6"></div>
            <div id="ebrd-content-menu-header">{"Case Study: " + data.case_header}</div>
            <div id="ebrd-content-menu-txt">{data.case_txt}</div>
            <div id="ebrd-content-alert">{"Click to explore"}</div>

            <div id="ebrd-content-links">
            {
              menu.map((a, i) => (
                <div
                  key={i}
                  id={`ebrd-content-link${i+1}`}
                  className={`ebrd-content-link`}
                  onClick={() => {
                    this.props.showPopup(data.id, i+1, true);
                  }}
                >
                  <Icon icon={`risks-icon${i+1}`} className="ebrd-content-link-icon" id={`ebrd-content-link-icon${i+1}`}/>
                  <div className="ebrd-content-link-text">{a}</div>
                  <div className="ebrd-content-link-subtext">{submenu[i]}</div>
                </div>
              ))
            }
            </div>

            <div id="ebrd-content-down-icons">
              <Icon icon={`content-down-icon${data.id}l`} className="ebrd-content-down-iconl" id={`ebrd-content-down-icon${data.id}l`}/>
              <Icon icon={`content-down-icon${data.id}r`} className="ebrd-content-down-iconr" id={`ebrd-content-down-icon${data.id}r`}/>
            </div>

          </div>


          <div className="content-go-up"
              onClick={() => {
                    TweenMax.to(window, 0.3, {scrollTo:0});
                  }}>
              <span>{"up"}</span>
              <Icon icon={`up`} className="content-go-up-icon"/>
          </div>
      </div>
    );
  }
}

