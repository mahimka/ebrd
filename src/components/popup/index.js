import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';
import 'gsap/TweenMax';

export default class Popup extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentDidUpdate(){
  }

  togglePopUp(i){

    let els = document.getElementsByClassName("ebrd-popup-example");
    for(var j=0, len=els.length; j<len; j++){
        if(i!=j){
          TweenMax.to(els[j], 0.25, {height:0})
          TweenMax.set(els[j], {marginTop:0})
        }
    }

    let elems = document.querySelectorAll(".ebrd-popup-bubble");

    [].forEach.call(elems, function(el, j) {
        if(i!=j)
          el.classList.remove("ebrd-popup-active");
    });

    el = document.getElementById("ebrd-popup-bubble" + i)
    let elex = el.getElementsByClassName("ebrd-popup-example")[0]
    let style = window.getComputedStyle(elex, null);

    if(style.height != "0px"){
      TweenMax.to(elex, 0.3, {height:0})
      TweenMax.set(elex, {marginTop:0})

    }else{
      TweenMax.set(elex, {height:"auto"})
      TweenMax.from(elex, 0.3, {height:0})
      TweenMax.set(elex, {marginTop:10})
    }

    el.classList.toggle("ebrd-popup-active");
  }

  render() { 
    const data = this.props;
    const colors = ["rgba(0,83,155,0.95)","rgba(0,121,193,0.95)","rgba(0,104,87,0.95)"];
    const headers = ["RISKS", "OPPORTUNITIES", "SCENARIO ANALAYSIS"];

    const popupStyle = {
      display: (this.props.showpopup)?'block':'none'
    }
    const popupContainerStyle = {
      backgroundColor: colors[this.props.n-1],
    }

    const Cross = ({ ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/cross.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    const Icon = ({ icon,  ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/${icon}.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    let els = document.getElementsByClassName("ebrd-popup-example");
    for(var j=0, len=els.length; j<len; j++){
        els[j].style["height"] = "0";
    }

    return (
      <div id="ebrd-popup" style={popupStyle}>
        <div id="ebrd-popup-container" style={popupContainerStyle} >
          <div id="ebrd-popup-subcontainer">
            

            <div id="ebrd-popup-subsubcontainer1">

              <div id="ebrd-whiteline7"></div>

              <div id="ebrd-popup-header">{headers[this.props.n-1] + ': ' + this.props.data["page"+this.props.id].case_header}</div>
              <div id="ebrd-popup-subheader">{"Disclosures about physical climate "} <span>{headers[this.props.n-1].toLowerCase()}</span></div>
              <div id="ebrd-popup-alert1" className="ebrd-popup-alert">{"Click the speech bubbles to find out more"}</div>
              <div id="ebrd-popup-alert2" className="ebrd-popup-alert">{"Touch the speech bubbles to find out more"}</div>

              <div id="ebrd-popup-bubbles">
                
                {
                  this.props.data["page"+this.props.id].disclosure[this.props.n-1].map((a, i) => (
                    <div
                      key={i}
                      id={`ebrd-popup-bubble${i}`}
                      className={`ebrd-popup-bubble ebrd-popup-bubble${this.props.n}`}
                      
                      onClick={() => {

                        this.togglePopUp(i);



                      }}
                    >
                      <div className="ebrd-popup-item">{a.item}</div>
                      <div className="ebrd-popup-example" dangerouslySetInnerHTML={{__html: a.example}} />
                     
                    </div>
                  ))
                }

              </div>

              <Cross
                id="ebrd-popup-cross"
                onClick={() => {
                  this.props.showPopup(1, 1, false);
                }}
              />              
              <Icon icon={`risks-popup-icon${this.props.n}`} className="ebrd-risks-popup-icon" id={`ebrd-risks-popup-icon${this.props.n}`}/>
            </div>

            <div id="ebrd-popup-subsubcontainer2">
              <div id="ebrd-popup-down-icons">
                <Icon icon={`popup-down-icon${this.props.id}l`} className="ebrd-popup-down-iconl" id={`ebrd-popup-down-icon${this.props.id}l`}/>
                <Icon icon={`popup-down-icon${this.props.id}r`} className="ebrd-popup-down-iconr" id={`ebrd-popup-down-icon${this.props.id}r`}/>
              </div>

              <Cross
                id="ebrd-popup-cross2"
                onClick={() => {
                  this.props.showPopup(1, 1, false);
                }}
              /> 
            </div>



          </div>
        </div>

      </div>
    );
  }
}