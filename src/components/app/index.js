import React from 'react';
import html from 'react-inner-html';
import Content from '../content';
import Popup from '../popup';
import Collbox from '../collbox';
import bind from '../../utils/bind';

import 'gsap/TweenMax';
import EasePack from 'gsap/EasePack'
import 'imports-loader?define=>false!gsap/src/uncompressed/plugins/AttrPlugin';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      current: 0,
      showpopup: false,
      id: 1,
      n: 1,
      collboxopen: (window.innerWidth > 800) ? true : false
    }

    this.st = document.documentElement.scrollTop;
    this.isAnimated = [false, false, false, false, false]

    this.href = document.location.pathname;

    bind(this, 'showPopup', 'collapse', 'startAnimation', 'stopAnimation', 'RenderPath');
  }

  componentDidMount() {
    let el = document.getElementById('app-root');
    setTimeout(function() {
      el.style.display = 'block';
      el.style.opacity = 1;
    }, 1000);


    let that = this
    window.onscroll = function() {
      var scrolled = window.pageYOffset || document.documentElement.scrollTop;
      that.setState({
        collboxopen: false
      });
    }

    let cll ='#ebrd-collapsable-box-window-content'
    if(!this.state.collboxopen){
      TweenMax.set(cll, {height:"36px"}) 
    }
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.current != this.state.current){
      TweenMax.set('.ebrd-mainpage', {opacity:0})
      TweenMax.set('.ebrd-content', {opacity:0})
      TweenMax.to('.ebrd-mainpage', 0.5, {opacity:1, ease:Power1.easeIn})
      TweenMax.to('.ebrd-content', 0.5, {opacity:1, ease:Power1.easeIn})
    }


    let cll ='#ebrd-collapsable-box-window-content'
    if(prevState.collboxopen != this.state.collboxopen){
        if(!this.state.collboxopen){
          TweenMax.to(cll, 0.3, {height:"36px"}) 
        }else{
          TweenMax.set(cll, {height:"auto"})
          TweenMax.from(cll, 0.3, {height:"36px"})
        }
    }
  }

  setCurrent(curr) {
    this.setState({
      current: curr,
      collboxopen: false
    });
  }

  showPopup(id, n, show) {
    this.setState({
      showpopup: show,
      id: id,
      n: n
    });


    if(show) this.st = document.documentElement.scrollTop;

    document.body.style.overflow = (show)?"hidden":"auto"
    document.body.style.position = (show)?"fixed":"relative"
    document.body.style.height = (show)?"100%":"auto"
    
    document.documentElement.style.overflow = (show)?"hidden":"auto"
    document.documentElement.style.position = (show)?"fixed":"relative"
    document.documentElement.style.height = (show)?"100%":"auto"

    if(!show) window.scrollTo(0, this.st);
  }

  collapse(show) {
    this.setState({
      collboxopen: show
    });
  }

  startAnimation(i){
    if(i==0){
      TweenMax.killTweensOf("#ebrd-boxes");
      TweenMax.fromTo("#ebrd-boxes", 3, {x:0}, {x:-22, repeat:-1, ease:Linear.easeNone});
    }
    if(i==1){
      TweenMax.killTweensOf("#ebrd-door");
      TweenMax.fromTo("#ebrd-door", 2, {y:0}, {y:-35, repeat:-1, yoyo: true, ease:Linear.easeNone}).repeatDelay(1);
    }
    if(i==2){
      TweenMax.killTweensOf("#ebrd-wheel");
      TweenMax.to("#ebrd-wheel",10,{rotation:360, transformOrigin:"center center", repeat: -1});
    }
    if(i==3){
      TweenMax.killTweensOf("#ebrd-shine");
      TweenMax.fromTo("#ebrd-shine", 3, {x:0, y:0}, {x:80, y:140, repeat:-1, delay: 0.2,  ease:Linear.easeNone}).repeatDelay(0.7);
    }
    if(i==4){
      TweenMax.killTweensOf("#ebrd-smoke");
      TweenMax.fromTo("#ebrd-smoke", 3, {x:30, y:-28}, {x:60, y:-80, repeat:-1, delay: 0.2,  ease:Linear.easeNone}).repeatDelay(0.7);
    }
  }

  stopAnimation(i){
    if(i==0){
      TweenMax.killTweensOf("#ebrd-boxes");
      TweenMax.to("#ebrd-boxes", 1, {x:0, ease:Linear.easeNone});
    }
    if(i==1){
      TweenMax.killTweensOf("#ebrd-door");
      TweenMax.to("#ebrd-door", 1, {y:0, ease:Linear.easeNone});
    }
    if(i==2){
      TweenMax.killTweensOf("#ebrd-wheel");
      TweenMax.to("#ebrd-wheel", 0, {rotation:0, ease:Linear.easeNone});
    } 
    if(i==3){
      TweenMax.killTweensOf("#ebrd-shine");
      TweenMax.set("#ebrd-shine", {x:0, y:0});
    } 
    if(i==4){
      TweenMax.killTweensOf("#ebrd-smoke");
      TweenMax.set("#ebrd-smoke", {x:0, y:0});
    } 
  }

  RenderPath(){
    const { data } = this.props;
    const menu = ["Manufacturing", "Commercial Property", "Agribusiness", "Mining", "Power & Energy"];

    const Icon = ({ icon,  ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/${icon}.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    const curr = this.state.current;
    let back = (curr != 1) ? (curr-1) : 5
    let forw = (curr != 5) ? (curr+1) : 1
    back = (back==-1) ? '5':back


    if(this.state.current == 0){
      return (
        <div className="ebrd-mainpage">
          <div className="ebrd-mainpage-header"> {data.mainpage.header} </div>
          <div className="ebrd-mainpage-texts">
            <div id="ebrd-whiteline1"></div>
            <div id="ebrd-whiteline2"></div>
            <div className="ebrd-mainpage-text"> {data.mainpage.txt0} </div>
            <br/>
            <div className="ebrd-mainpage-text"> {data.mainpage.txt1} </div>
          </div>
          <div className="ebrd-mainpage-menu">
            <div id="ebrd-whiteline3"></div>
            <div id="ebrd-alert">{"Click on the illustrations to explore"}</div>
            {
              menu.map((a, i) => (
                <a
                  key={i}
                  to={this.href + `/${i+1}`}
                  id={`ebrd-link${i}`}
                  className={`ebrd-link`}
                  onMouseOver={() => {
                    this.startAnimation(i)
                  }}
                  onMouseOut={() => {
                    this.stopAnimation(i)
                  }}

                  onClick={() => {
                    this.setCurrent(i+1);
                  }}
                >
                  <Icon icon={`icon${i+1}`} className="ebrd-icon" id={`ebrd-icon${i+1}`}/>
                  <div className="ebrd-link-text">{a}</div>
                </a>
              ))
            }
          </div>

          <div className="content-go-up"
              onClick={() => {
                    TweenMax.to(window, 0.3, {scrollTo:0});
                  }}>
              <span>{"up"}</span>
              <Icon icon={`up`} className="content-go-up-icon"/>
          </div>
          
        </div>
        )
    }else{
      return <Content {...data['page'+this.state.current]} showPopup={this.showPopup} />
    }

  }


  render() {
    const { data } = this.props;
    const Icon = ({ icon,  ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/${icon}.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    const curr = this.state.current;
    let back = (curr != 1) ? (curr-1) : 5
    let forw = (curr != 5) ? (curr+1) : 1
    back = (back==-1) ? '5':back

    return (

        <div id="ebrd-infographic">
          
          <Collbox {...this.state}  collapse={this.collapse} />

          <div id="ebrd-menu-box">
            <div id="ebrd-menu-box-window">
              <a onClick={() => {this.setCurrent((back=='')?0:back);}}>
                <Icon icon={`back`} id={`ebrd-back`}/>
              </a>
              <a onClick={() => {this.setCurrent(0);}}>
                <div  id={`ebrd-home`}>Home</div>
              </a>
              <a onClick={() => {this.setCurrent((forw=='')?0:forw);}}>
                <Icon icon={`forw`} id={`ebrd-forw`}/>
              </a>
            </div>
          </div>

          

          {this.RenderPath()}

          <Popup {...this.state} showPopup={this.showPopup} data={data}/>
          
        </div>
    );
  }
}