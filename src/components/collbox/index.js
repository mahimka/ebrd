import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';

export default class Collbox extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentDidUpdate(){
  }

  render() { 

    const Icon = ({ icon,  ...props }) => {
      const svg = require(`!raw-loader!../../assets/img/${icon}.svg`);
      return <span {...props} dangerouslySetInnerHTML={{__html: svg}}/>;
    };

    return (
      <div id="ebrd-collapsable-box" className={(this.props.current==0)?"ebrd-collapsable-box-fixed":"ebrd-collapsable-box-absolute"}>
        <div id="ebrd-collapsable-box-window">
          <div id="ebrd-collapsable-box-window-content" className={(this.props.collboxopen) ? "ebrd-collapsable-opened" : "ebrd-collapsable-closed"}>
            <div id="ebrd-collapsable-box-window-header">{"Get The Report"}</div>
            <div id="ebrd-collapsable-box-window-txt">{"‘Advancing TCFD guidance on Physical Climate Risk And Opportunities‘"}</div>
            <div id="ebrd-collapsable-box-window-line"></div>
            <div 
              id="ebrd-collapsable-box-window-download"
              onClick={() => {
                window.open('https://s3.eu-west-2.amazonaws.com/ebrd-gceca/EBRD-GCECA_draft_final_report_full_2.pdf', '_blank');
              }}
            >
              <div id="ebrd-collapsable-box-window-download-txt">{"DOWNLOAD"}</div>
              <div id="ebrd-collapsable-box-window-download-arrow"><Icon icon={`download`}/></div>
            </div>
            <div id="ebrd-collapsable-box-sharing">
              
              <div id="ebrd-collapsable-box-window-shareit">{"share it"}</div>
              <div id="ebrd-share-tw" className="ebrd-share"><Icon icon={`tw`}/></div>
              <div id="ebrd-share-in" className="ebrd-share"><Icon icon={`in`}/></div>
              <div id="ebrd-share-wu" className="ebrd-share"><Icon icon={`wu`}/></div>
              <div id="ebrd-share-fb" className="ebrd-share"><Icon icon={`fb`}/></div>

            </div>

          </div>
          <div 
            id="ebrd-collapsable-box-open-btn"
            onClick={() => {
              this.props.collapse(!this.props.collboxopen);
            }}
          >
            <div id="ebrd-collapsable-box-open-btn-txt">{(this.props.collboxopen) ? "see less" : "see more"}</div>
            <div id="ebrd-collapsable-box-open-btn-icon"><Icon icon={(this.props.collboxopen) ? "less" : "more"}/></div>
          </div>
        </div>
      </div>
    );
  }
}

